<?php
//includes the file that connects and selects the database
require 'connect.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Admin</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>

<script type="text/javascript">
function startTime()
{

   var today=new Date();
   var h=today.getHours();
   var m=today.getMinutes();
   var s=today.getSeconds();
   
//add zero infront of numbers <10
   m=checkTime(m);
   s=checkTime(s);

       document.getElementById('txt').innerHTML=h+":" +m+":"+s; //formats the display
   
   t=setTimeout('startTime()',500);

function checkTime(i)
  {
   if(i<10)
     {
       i="0"+i
     }
return i;
  }
}
</script>
<script type="text/javascript">
//--------------- LOCALIZEABLE GLOBALS ---------------
var d=new Date();
var monthname=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
//Ensure correct for language. English is "January 1, 2004"
var TODAY = monthname[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body onload= "startTime()">
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
   <div id="admin_head">Sam_Ndi Gymnasium System</div>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
          <div class="wrap block-2">
            <div class="col-1_profile">

  <!--Main Body-->
  <div class="menu_border">
           <span class="dateformat"> 
              <script language="JavaScript" type="text/javascript">
                    document.write('<b>Today: </b>');  
                    document.write(TODAY);
              </script>
              <b>Time:</b><span id="txt"></span>
            </span>
    <ul class="menu_admin">
        <li class="current"><a href="admin_menu.php">Home</a></li>
        <li><a href="users.php">Members</a></li>
        <li><a href="view_bookings.php">Bookings</a></li>
        <li><a href="inventory.php">Equipment</a></li>
        <li><a href="payments.php">Payments</a></li>
        <li><a href="staff.php">Staff</a></li>
        <li><a href="adminforum.php">Forum</a></li>
  </ul> <br><br>
              <?php 
                $user=$_SESSION['logged_user'];
                $query="SELECT lname,image_name,path FROM users WHERE email='$user'";
                $result = mysql_query($query);
                if(!$result)
                    {
                          echo "Error ".mysql_error();
                    }
                else{

                
                 while($row=mysql_fetch_array($result)){
                   $fname=$row['lname'];
                   echo "<div id='logged_admin'>Welcome <u><i>".$fname."</i></u>"."</div>"."<br>";
                    ?>
            <br><br>
             <form action="log_out.php" method="post">
                 <input type="submit" id="logout_admin" value="Log Out">
              </form> 
<img src="<?php echo $row['path'];?>" class="circleborder" align="right"><br><br><br><br>
            <?php
               }
              }
              ?>
                
              <div class="admin_menu">
                
              </div>
              <div id="admin_option">
                <u>Admin Quick Menu</u>
                <ul>
                  <li class="quick_menu"><a href="register.php">Loggin as different admin</a></li>
                  <li class="quick_menu"><a href="users.php">Change Password</a></li>
                  <li class="quick_menu"><a href="users.php">Update my Details</a></li>
                  <li class="quick_menu"><a href="profile.php">View My Profile</a></li>
                  <li class="quick_menu"><a href="index.php">Sam_Ndi Gym Home</a></li>
                  <li class="quick_menu"><a href="forum.php">Member Forum</a></li>
                </ul>

              </div>
              
            </div>
          </div>
           
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
   <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
