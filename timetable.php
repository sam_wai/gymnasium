<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Timetable</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li class="current"><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1">
              <h2 class="p3"><span class="color-1">Our</span> timetable</h2>
              <div class="wrap box-2 border-1">
                <div>
                  <p><strong>Monday</strong></p>
                  <p class="color-1">9:30</p>
                  <p><strong><u>Chest,Back,Shoulders,And Lower Abs</u></strong></p>
                  <ul>
                    <li>&#8226;75 Degree Incline Dumbbell Press</li>
                    <li>&#8226;One Arm Dumbbell Rows</li>
                    <li>&#8226;Flat Dumbbell Press</li>
                    <li>&#8226;Dumbell Curl</li>
                    <li>&#8226;Incline Curls </li>
                  </ul>
                </div>
                <div>
                  <p><strong>Wednesday</strong></p>
                  <p class="color-1">10:00</p>
                  <p><strong><u>Upper Legs, Calves, Abs, Shoulders </u></strong></p>
                  <ul>
                    <li>&#8226;Squats</li>
                    <li>&#8226;Lunges</li>
                    <li>&#8226;Leg Extensions</li>
                    <li>&#8226;Stiff Legged Deadlifts</li>
                    <li>&#8226;Leg Curls</li>
                    <li>&#8226;Calf Raises</li> 
                  </ul>
                </div>
                <div class="last">
                  <p><strong>Friday</strong></p>
                  <p class="color-1">9:30</p>
                  <p><strong><u>Shoulders and Arms  </u></strong></p>
                  <ul>
                    Giant set:
                      <li>&#8226;Dumbbell Upright Rows</li>
                      <li>&#8226;Lying Triceps Extensions</li>
                      <li>&#8226;Overhead Triceps Extensions</li>
                      <li>&#8226;Incline Curls </li>
                  </ul>
                </div>
              </div>
              <div class="wrap box-2 border-1">
                <div>
                  <p class="color-1">12:00</p>
             <p><strong><u>Cross-Country Run </u></strong></p>
                   A 2km run around the town and back.
                </div>
                <div>
                  <p class="color-1">12:30</p>
                  <p><strong><u>Chest And Back   </u></strong></p>
                  <ul>
                    <li>&#8226;Two Arm Dumbbell Rows with Reverse Grip (Palms Up)</li>
                     <li>&#8226;45 Incline Dumbbell Bench Press</li>
                     <li>&#8226;Dumbbell Pullovers</li> 
                  </ul>
                </div>
                <div class="last">
                  <p class="color-1">11:00</p>
                  <p><strong><u>Chest, Shoulders And Back  </u></strong></p>
                  <ul>
                    <li>&#8226;Seated Front Press</li> 
                    <li>&#8226; tri-setted with</li>
                    <li>&#8226;Wide Grip Pulldowns</li> 
                    <li>&#8226;Standing Bentover Lateral Raises  </li>
                  </ul>
                </div>
              </div>
              <div class="wrap box-2">
                <div>
                  <p class="color-1">18:00</p>
                <p><strong><u>Upper Body </u></strong></p> 
                   <ul>
                    <li>&#8226;Seated Front Press </li>
                    <li>&#8226;supersetted with </li>
                    <li>&#8226;Pulldowns </li>
                    <li>&#8226;Dumbbell Inclines </li>
                    <li>&#8226;supersetted with </li>
                    <li>&#8226;Pullovers </li> 
                   </ul> 
                </div>
                <div>
                  <p class="color-1">17:30</p>
                  <p><strong><u>Legs </u></strong></p>
                     <ul>
                      <li>&#8226;Leg Extensions </li>
                     <li>&#8226;Leg Curls </li>
                     <li>&#8226;Calf Raises </li>
                     <li>&#8226;Squats </li>
                     <li>&#8226;Deadlifts </li> 
                     </ul>
                </div>
                <div class="last">
                  <p class="color-1">17:00</p>
                   <p><strong><u>Cross-Country Run </u></strong></p>
                   A 2km run around the town and back.
                </div>
              </div>
            </div>
            <div class="col-2">
              <h2 class="p3"><span class="color-1">Events</span> Schedule</h2>
              <p class="p2"><strong>Look at the timetable,Make a schedule!</strong></p>
              <p>As we try to ensure that the exercise time is convenient to you,we allow you to organise youself and give us your availability.We also offer make-up classes as listed:</p>
              <img src="images/page3-img1.jpg" alt="" class="img-border pad-1">
              <ul class="list-2">
                <li><a href="#">Every Tuesday at 6.00pm evenings.</a></li>
                <li><a href="#">Every Thursday at 5.30am morning.</a></li>
                <li><a href="#">Saturday from 4.30am to 11.00 am.</a></li>
              </ul>
              
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
