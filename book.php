<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Booking</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<link href="css/htmlDatePicker.css" rel="stylesheet">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<script src="js/htmlDatePicker.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li class="current"><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1">
              <h2 class="p3"><span class="color-1">Book An Appointment</span> In Our Gymnasium</h2><br><br>
               <div id="table">
               <table border="0" cellpadding="2" cellspacing="7" width="110%">

                <p style="font-size: 15px;font-family: tahoma;">Fill in the following details to book an day with us.You may also <a href="register.php">register</a> and become a member to be attending our classes and be assigned a personal nutritionist.</p><br>
                         <form name="book" method="post" action="book_vali.php">
                          <legend style="font-weight: bold;font-style :italic;">Please Fill in The following details:</legend><br>
                             <tr><td class="space">First Name</td><td><div id="inputs"><input type="text" required="required" maxlength="60"  name="fir_name" /></div></td></tr>
                             <tr><td class="space">Second Name</td><td><div id="inputs"><input type="text" required="required" maxlength="60"  name="sec_name" /></div></td></tr>
                             <tr><td class="space">Email Address</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email"/></div></td></tr>
                             <tr><td class="space">Phone no.</td><td><div id="inputs"><input type="text" required="required" maxlength="15"  name="phone" /></div></td></tr>
                             <tr><td class="space">Date </td><td>
                                <script><!--
                                        var today = new Date();
                                        dateFormat = "Y/m/d";
                                        DisablePast = true;
                                        range_start = new Date(today.getFullYear(),today.getMonth(),1);
                                        range_end = new Date(today.getFullYear(),today.getMonth(),5);
                                  --></script>
                                  <input type="text" placeholder="Select date" name="date_from" id="date_area" readonly onClick="GetDate(this);">
                                  </td></tr>
                             <tr><td class="space">Purpose</td><td>
                                      <select name="purpose"> 
                                         <option selected="selected">Select</option> 
                                         <option>Training</option> 
                                         <option>Check Up</option>
                                         <option>Official Visit</option>
                                         <option>Show Around</option>
                                      </select></td></tr>
                            <tr><td class="space">Other Reasons (specify)</td><td >
                              <div id="t_area">
                                <textarea name="details" cols="30" rows="4"></textarea>
                              </div></td></tr>
                            <tr><td colspan="2" align="center"><br />
                              <div id="butt"><button type="reset" id="actions">Clear</button>
                              <button type="submit" id="actions">Submit</button></div></td></tr>
                        </form>
                    </table>
                  </div>
            </div>
            <div class="col-2">
              <h2 class="p6"><span class="color-1">Services</span> Offered</h2><br>
              <img src="images/page4-img2.jpg" alt="" class="img-border">
              <p class="p2 top-6"><strong>List of our Services</strong></p>
              <ul class="list-2">
                <li>Main work our area</li>
                <li>Cardio area/theatre</li>
                <li>Group exercise classes</li>
                <li>Sports facilities</li>
                <li>Child care facilities</li>
              </ul>
           </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
