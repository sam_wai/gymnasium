<?php
//includes the file that connects and selects the database
require 'connect.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Admin</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset1.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>

<script type="text/javascript">
function startTime()
{

   var today=new Date();
   var h=today.getHours();
   var m=today.getMinutes();
   var s=today.getSeconds();
   
//add zero infront of numbers <10
   m=checkTime(m);
   s=checkTime(s);

       document.getElementById('txt').innerHTML=h+":" +m+":"+s; //formats the display
   
   t=setTimeout('startTime()',500);

function checkTime(i)
  {
   if(i<10)
     {
       i="0"+i
     }
return i;
  }
}
</script>
<script type="text/javascript">
//--------------- LOCALIZEABLE GLOBALS ---------------
var d=new Date();
var monthname=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
//Ensure correct for language. English is "January 1, 2004"
var TODAY = monthname[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body  onload= "startTime()">
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
   <div id="admin_head">Sam_Ndi Gymnasium System</div>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
          <div class="wrap block-2">
            <div class="col-1_profile">
             
          <div class="menu_border">
            <span class="dateformat"> 
              <script language="JavaScript" type="text/javascript">
                    document.write('<i><b>Today </b></i>');  
                    document.write(TODAY);
              </script>
              <i><b>Time</b></i><span id="txt"></span>
            </span><br><br>

              <form action="users.php" method="post">
                 <input type="submit" id="logout_admin" value="Back">
              </form><br><br><br>

            <span id="welcome1"><u>All Pending Members :</u></span><br><br>
                <table cellspacing="5" cellpadding="2" width="90%" name="bookns"> 
                   <tr><th width="5%">ID</th><th width="10%">First Name</th><th>Last Name</th><th>Phone No.</th><th width="24%">Email Address</th><th>Gender</th><th width="%">Joined On.</th><th width="10%">Action</th></tr>
                </table>
  <!--function to display all the users from the database-->


              <?php

                  $query="SELECT * FROM users WHERE mem_status='Pending'";
                     $result = mysql_query($query);
                        if(!$result)
                         {
                          echo "Error ".mysql_error();
                         }

                       else{

                     while($row=mysql_fetch_array($result))
                     {
                      $member=$row['user_id'];
                  ?>
                   <table width="90%" cellspacing="5" cellpadding="2" name="bookns"> 
                   <tr><td width="8%"><?php echo $row['user_id'];?></td><td width="12%"><?php echo $row['fname'];?></td><td width="12%"><?php echo $row['lname'];?></td><td width="15%"><?php echo $row['phone'];?></td><td width="24%"><?php echo $row['email'];?></td><td width="10%"><?php echo $row['gender'];?></td><td width="14%"><?php echo $row['date_joined'];?></td><td width="10%"><?php //echo "<a href='cancelled.php?id=$member'>Cancel</a>";?> <?php echo "<a href='confirm_member.php?id=$member'>Confirm</a>";?></td></tr>
                </table>
              <?php 
             }
           }
        ?>
              
            </div>
           </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
   <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
