<?php
//includes the file that connects and selects the database
require 'connect.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | New_Equipment</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<link href="css/htmlDatePicker.css" rel="stylesheet">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<script src="js/htmlDatePicker.js"></script>

<script type="text/javascript">
function startTime()
{

   var today=new Date();
   var h=today.getHours();
   var m=today.getMinutes();
   var s=today.getSeconds();
   
//add zero infront of numbers <10
   m=checkTime(m);
   s=checkTime(s);

       document.getElementById('txt').innerHTML=h+":" +m+":"+s; //formats the display
   
   t=setTimeout('startTime()',500);

function checkTime(i)
  {
   if(i<10)
     {
       i="0"+i
     }
return i;
  }
}
</script>
<script type="text/javascript">
//--------------- LOCALIZEABLE GLOBALS ---------------
var d=new Date();
var monthname=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
//Ensure correct for language. English is "January 1, 2004"
var TODAY = monthname[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body onload= "startTime()">
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
   <div id="admin_head">Sam_Ndi Gymnasium System</div>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
          <div class="wrap block-2">
            <div class="col-1_profile">
              
          <div class="menu_border">
            <span class="dateformat"> 
              <script language="JavaScript" type="text/javascript">
                    document.write('<i><b>Today </b></i>');  
                    document.write(TODAY);
              </script>
              <i><b>Time</b></i><span id="txt"></span>
            </span><br><br>

                  <form action="inventory.php" method="post">
                      <input type="submit" id="logout_admin" value="Back">
                  </form>

<?php
   if(isset($_POST['submit']))
   {
      if ($_POST['name'] != "" || $_POST['category'] != "" || $_POST['total'] != "" || $_POST['supplier'] != "" || $_POST['cost'] != "" || $_POST['manage'] != "")
       {
   
   if($_FILES['file']["type"]=="image/png"||$_FILES['file']["type"]=="image/jpeg"||$_FILES['file']["type"]=="image/jpg"||$_FILES['file']["type"]=="image/gif"){ 
   
   
   $name=mysql_real_escape_string(basename($_FILES['file']['name']));
   $t_name=$_FILES['file']['tmp_name'];
   $dir='upload';
   //$body=$_POST['body']; 
      if(move_uploaded_file($t_name,$dir."/".$name)){
      
   }
   else{
      echo 'upload failed';
  } 
     $querry=mysql_query("INSERT INTO inventory (equip_name,category,max_count,purchase_date,supplier,cost,department,staff_no,image_name,path)
             VALUES ('$_POST[name]','$_POST[category]','$_POST[total]','$_POST[purchased]','$_POST[supplier]','$_POST[cost]','$_POST[dept]','$_POST[manage]','$name','upload/$name')");
     //catches the errors
    if(!$querry)
        {
           echo "Could not insert ".mysql_error();

        }



        else
          {
          echo "<script> alert('Item added successfully!!'); </script>";
     }  
    }
   }
 }
?>

                     <br><br>
                     <div id="table">
                       <table border="0" cellpadding="2" cellspacing="7" width="80%">
                         <form name="equipment"method="post" action="" enctype="multipart/form-data">
                             <tr><td  class="space">Equipment Name</td><td><div id="inputs"><input type="text" required="required" maxlength="16"
                              name="name" /></div></td></tr>
                             
                             <tr><td class="space">Total Items</td><td><div id="inputs"><input type="text" required="required" maxlength="60"  name="total"/></div></td></tr>
                            
                          <tr><td  class="space">Buying Cost</td><td><div id="inputs"><input type="text" required="required" maxlength="16"
                              name="cost" /></div></td></tr>
                          <tr><td class="space">Purchased On:</td><td>
                                <script type="text/javascript"><!--
                                    var today = new Date();
                                    dateFormat = "Y/m/d";
                                        DisablePast = false;
                                        range_start = new Date(today.getFullYear(),today.getMonth(),8);
                                        range_end = new Date(today.getFullYear(),today.getMonth(),5);
                                  --></script>
                              <input type="text" placeholder="Select date" name="purchased" id="date_area" readonly onClick="GetDate(this);">
                          </td></tr>
                          <tr><td class="space">Category</td><td>
                                        <select name="category"> 
                                         <option selected="selected">Category</option> 
                                         <option>Free Weight</option> 
                                         <option>Machine</option>
                                         <option>Accesories</option>
                                      </select>
                          </td></tr>

                          <tr><td class="space">Department</td><td>
                                        <select name="dept"> 
                                         <option selected="selected">Choose</option> 
                                         <option>Sports</option> 
                                         <option>Children Area</option>
                                         <option>Sports Arena</option>
                                      </select>
                            </td></tr>
                          <tr><td class="space">Care of Staff id:</td><td>
                                        <select name="manage"> 
                                         <option selected="selected">Choose</option> 
                                         <option>1</option> 
                                         <option>2</option>
                                         <option>3</option>
                                         <option>4</option>
                                         <option>5</option>
                                         <option>6</option>
                                         <option>7</option>
                                      </select>
                            </td></tr>
                            <tr><td class="space">Supplier</td><td>
                                        <select name="supplier"> 
                                         <option selected="selected">Choose</option> 
                                         <option>Kamau Stores</option> 
                                         <option>East African Gym</option>
                                         <option>Amazon Online Dealers</option>
                                      </select>
                            </td></tr>
                            <tr><td class="space">Equipment Image</td><td><input type="file" name="file" /></div></td></tr>
                            <tr><td colspan="2" align="center"><br />
                              <button type="submit" id="actions" name="submit">Add Now</button></td></tr>
                         </form>
                      </table>
                    </div>
            </div>
           </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
   <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
