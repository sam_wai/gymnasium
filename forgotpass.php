<?php
//includes the file that connects and selects the database
require 'connect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Password</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li class="current"><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1">
              <h2 class="p3"><span class="color-1">Reset </span> Your Password</h2><br><br>

              <form action="register.php" method="post">
                 <input type="submit" id="logout_admin" value="Login">
              </form>
                <br><br>
               <div class="email_form">
               <table border="0" cellpadding="2" cellspacing="7" width="80%">
                  <form method="post" action="" >
                      <tr><td class="space">Your Email</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email"/></div></td></tr>
                        <tr><td colspan="2" align="right">
                              <button type="submit" class="search" name="submit">Search</button></td></tr>
                  </form>
              </table>
              <?php

              if(isset($_POST['submit']))
               {
      
                $email=$_POST['email'];

                  $find="SELECT email,fname,lname FROM users WHERE email='$email'";

                  $result=mysql_query($find) or die(mysql_error());


              if(mysql_num_rows($result)==0)
                       {
                        echo "<div class='wrong_email'>The Email Address Entered is Invalid!!</div>";
                       }
             else
                 {
                  function randomPass($length)
                        {
                     $password="";
                     $length=$length;
                     $chars="abcdefghijklmnopqrstuvwxyz123456789";

                    for($x=1;$x<=$length;$x++)
           
                     {
                       $rand=rand() %strlen($chars);
                       $temp=substr($chars,$rand,1);
                       $password.=$temp;
                     }
                 return $password;
               }
  
  
                $pass=randomPass(7);
               
                  $query="UPDATE users SET password='$pass' WHERE email='$email'";
                  $result1 = mysql_query($query);
                   if(!$query)
                        {
                            echo "Failed to update ".mysql_error();
                         }

                  while($row=mysql_fetch_array($result))
                     {
                      $name=$row['fname'];

                  $to = $email;
                  $user=$name;
                  $subject = "New Login Password";
                  $header="Admin sam_ndi gymnasiums";
                  $message = "Hello $user ,\nYour New Password is $pass .\n\nPlease call us if your are not aware of the password change.\n\n\nThis is automatically sent Email.Please do not reply.";
                  
                  mail($to,$subject,$message,$header);
                      
                      echo "<div class='showname'>Hello ".$row['fname'] ." ".$row['lname']."</div>";
                      echo "<div class='showdetails'>New <span style='color:#ff6666;font-style:italic;'>Password</span> has been sent to Your Email.Please use it to Login now.</div>";
                  

                 }
               }
             }

              ?>

              </div>
            </div>
            <div class="col-2">
          
               </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
