<?php
//includes the file that connects and selects the database
require 'connect.php';
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Inventory</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>

<script type="text/javascript">
function startTime()
{

   var today=new Date();
   var h=today.getHours();
   var m=today.getMinutes();
   var s=today.getSeconds();
   
//add zero infront of numbers <10
   m=checkTime(m);
   s=checkTime(s);

       document.getElementById('txt').innerHTML=h+":" +m+":"+s; //formats the display
   
   t=setTimeout('startTime()',500);

function checkTime(i)
  {
   if(i<10)
     {
       i="0"+i
     }
return i;
  }
}
</script>
<script type="text/javascript">
//--------------- LOCALIZEABLE GLOBALS ---------------
var d=new Date();
var monthname=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
//Ensure correct for language. English is "January 1, 2004"
var TODAY = monthname[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body onload= "startTime()">
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
   <div id="admin_head">Sam_Ndi Gymnasium System</div>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
          <div class="wrap block-2">
            <div class="col-1_profile">
         
         <div class="menu_border">
               <span class="dateformat"> 
              <script language="JavaScript" type="text/javascript">
                    document.write('<i><b>Today </b></i>');  
                    document.write(TODAY);
              </script>
              <i><b>Time</b></i><span id="txt"></span>
            </span>

           <ul class="menu_admin">
               <li><a href="admin_menu.php">Home</a></li>
               <li><a href="users.php">Members</a></li>
               <li><a href="view_bookings.php">Bookings</a></li>
               <li class="current"><a href="inventory.php">Equipment</a></li>
               <li><a href="payments.php">Payments</a></li>
               <li><a href="staff.php">Staff</a></li>
               <li><a href="adminforum.php">Forum</a></li>
           </ul> <br><br>
              
              <form action="admin_menu.php" method="post">
                 <input type="submit" id="logout_admin" value="Main Menu">
              </form><br><br><br><br>
        
              <form action="" method="post">
                 <div id="input">View Equipment Details: <input type="text" name="equipt-id" placeholder="equipt-id"></div>
                 <input type="submit" name="submit" class="actions" value="View">
              </form><br>

              <form action="" method="post" id="table_2">
                 <div id="input">Remove From the Stores: <input type="text" name="item" placeholder="equipt-id"></div>
                 <input type="submit" class="actions" name="del"value="Remove" onclick="return confirm('Are you sure you want to DELETE the user?');">
              </form><br>

              <form action="" method="post" id="table_3">
                 <div id="input">Update Service Date: <input type="text" name="user-id" placeholder="equipt-id"></div>
                 <input type="submit" class="actions" value="Update" name="upd">
              </form><br>
              
              <a href="newequipt.php" class="new_2">Add Item to the store</a><br>
  <!--function to search and display -->
              <?php
              if(isset($_POST['submit']))
              {      
                    $id=$_POST['equipt-id'];

                       $find="SELECT equip_id FROM inventory WHERE equip_id='$id'";
                       $show="SELECT inventory.*,staff.staff_no,staff.first_name,staff.sec_name 
                              FROM inventory 
                              LEFT JOIN staff
                              ON  staff.staff_no=inventory.staff_no 
                              WHERE equip_id=$id";

                       $result1=mysql_query($find) or die(mysql_error());
                       $result=mysql_query($show) or die(mysql_error());

                       if(mysql_num_rows($result1)==0)
                       {
                        echo "<script> alert('Item Doesnt Exist!! Either Deleted or Yet Added!');</script>";
                       }
                  
                    while($row=mysql_fetch_array($result))
                         {
                          $itemid=$row['equip_id'];
                          ?>
                          <div id="result">
                          <span id="welcome1">Item Id NO: <?php echo $id;?></span>
                  <img src="<?php echo $row['path'];?>" height="180px" width="200px" class="img-border pad-1" align="right"><br><br>
                     <b>Item Name:</b> <?php echo $row['equip_name'];?><br>
                     <b>Category:</b> <?php echo $row['category'];?> <br>
                     <b>Department:</b> <?php echo $row['department'];?><br>
                     <b>Count in Stock:</b> <?php echo $row['max_count'];?><br>
                     <b>Purchased On:</b> <?php echo $row['purchase_date'];?><br>
                     <b>Last Serviced On:</b> <?php echo $row['last_service_date'];?> <?php //  echo "<a href='updatedate.php?update=$itemid'>Update Now</a>";?><br> 
                     <b>Under Care Of:</b> <?php echo $row['first_name'];?><br>
          
                      </div>
                         <?php
                         }

               }
                ?>
                <br><br>
  <!--function to delete-->
          <?php
               if(isset($_POST['del']))
               {
      
                $item=$_POST['item'];

                  $find="SELECT equip_id FROM inventory WHERE equip_id='$item'";
                  $result1=mysql_query($find) or die(mysql_error());

                  $delete="DELETE from inventory WHERE equip_id='$item'";
                  $result = mysql_query($delete);

              if(mysql_num_rows($result1)==0)
                       {
                        echo "<script> alert('Item Doesnt Exist!!');</script>";
                       }
             else
                 {

                  echo  "<script> alert('Item Removed Successfully!');</script>";
                 }
               }
          ?>
<!--function to put new values-->
          <?php
               if(isset($_POST['apt-values']))
               {
      
                $id=$_POST['user-id'];


                  $query="UPDATE users SET fname='$_POST[first]',lname='$_POST[last]',password='$_POST[pass1]',phone='$_POST[phone]',gender='$_POST[gender]' WHERE equip_id='$id'";
                  $result = mysql_query($query);
              if(!$query)
                       {
                        echo "Failed to update ".mysql_error();
                       }
             else
                 {

                  echo  "<script> alert('User profile updated Successfully!');</script>";

                  $show="SELECT users.*,more_details.* 
                              FROM users 
                              LEFT JOIN more_details
                              ON  users.equip_id=more_details.equip_id 
                              WHERE users.equip_id=$id";
                  $result=mysql_query($show) or die(mysql_error());
                   while($row=mysql_fetch_array($result))
                     {
                  ?>
                  
                  <div id="result1">
                    <span id="welcome1">New Updated Details For User Id NO: <?php echo $id;?>!</span><br><br>
                   <img src="<?php echo $row['path'];?>" height="180px" width="200px" class="img-border pad-1" align="right"><br><br>
                     <b>Name:</b> <?php echo $row['fname'];?> <?php echo $row['lname'];?><br>
                     <b>Phone:</b> <?php echo $row['phone'];?> <br>
                     <b>Email:</b> <?php echo $row['email'];?><br>
                     <b>Gender:</b> <?php echo $row['gender'];?><br>
                     <b>Weight:</b> <?php echo $row['weight'];?><br> 
                     <b>Height:</b> <?php echo $row['height'];?><br>
                     <b>BMI status:</b> <?php echo $row['BMI'];?><br>
                     <b>Fee Balance:</b> <?php echo $row['Balance'];?><br>
                     <b>Membership Status:</b> <?php echo $row['mem_status'];?><br>
                     <b>Joined On:</b> <?php echo $row['date_joined'];?><br>
                      </div>
                 <?php 
                   }
                 }
               }
          ?>
            
<span id="welcome1"><u>Sam_Ndi Gymnasium Equipment Store :</u></span><br><br>
           <?php

                  $query="SELECT inventory.*,staff.first_name
                          FROM inventory
                          LEFT JOIN staff
                          ON staff.staff_no=inventory.staff_no ";

                     $result = mysql_query($query);
                        if(!$result)
                         {
                          echo "Error ".mysql_error();
                         }

                       else{

                     while($row=mysql_fetch_array($result))
                     {

                      $itemid=$row['equip_id'];
                  ?>
                  <div id="result">
                   <img src="<?php echo $row['path'];?>" height="180px" width="200px" class="img-border pad-1" align="right"><br><br>
                     <b>Item No: <?php echo $row['equip_id'];?></b> <br>
                     <b>Item Name:</b> <?php echo $row['equip_name'];?><br>
                     <b>Count in Store:</b> <?php echo $row['max_count'];?> <br>
                     <b>Purchased On:</b> <?php echo $row['purchase_date'];?><br>
                     <b>Last Service On:</b> <?php echo $row['last_service_date'];?><br> 
                     <b>Under Care of:</b> <?php echo $row['first_name'];?><br> 
                      </div>
              <?php 
             }
           }
        ?>
           </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
   <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
