<?php
//includes the file that connects and selects the database
require 'connect.php';
session_start();
$sum=30000;

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Users</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>

<script type="text/javascript">
function startTime()
{

   var today=new Date();
   var h=today.getHours();
   var m=today.getMinutes();
   var s=today.getSeconds();
   
//add zero infront of numbers <10
   m=checkTime(m);
   s=checkTime(s);

       document.getElementById('txt').innerHTML=h+":" +m+":"+s; //formats the display
   
   t=setTimeout('startTime()',500);

function checkTime(i)
  {
   if(i<10)
     {
       i="0"+i
     }
return i;
  }
}
</script>
<script type="text/javascript">
//--------------- LOCALIZEABLE GLOBALS ---------------
var d=new Date();
var monthname=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
//Ensure correct for language. English is "January 1, 2004"
var TODAY = monthname[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
</script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body onload= "startTime()">
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
   <div id="admin_head">Sam_Ndi Gymnasium System</div>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
          <div class="wrap block-2">
            <div class="col-1_profile">
              
          <div class="menu_border">
               <span class="dateformat"> 
              <script language="JavaScript" type="text/javascript">
                    document.write('<i><b>Today </b></i>');  
                    document.write(TODAY);
              </script>
              <i><b>Time</b></i><span id="txt"></span>
            </span>

      <ul class="menu_admin">
        <li><a href="admin_menu.php">Home</a></li>
        <li class="current"><a href="users.php">Members</a></li>
        <li><a href="view_bookings.php">Bookings</a></li>
        <li><a href="inventory.php">Equipment</a></li>
        <li><a href="payments.php">Payments</a></li>
        <li><a href="staff.php">Staff</a></li>
        <li><a href="adminforum.php">Forum</a></li>
    </ul> <br><br>
              <form action="admin_menu.php" method="post">
                 <input type="submit" id="logout_admin" value="Main Menu">
              </form><br><br>
                <div id="summary">
                  <span class="summary_headn">In Summary:</span><br><br>
                  <?php
                          $pending="SELECT COUNT(mem_status) AS Pending FROM users WHERE mem_status='pending'";
                          $result1=mysql_query($pending) or die(mysql_error());

                               while($row=mysql_fetch_array($result1))
                                {
                                echo "<b>"."Total Pending Members: "."</b>".$row['Pending'];
                                }
                     ?>
                     <br>
                     <?php
                          $confirmed="SELECT COUNT(mem_status) AS Confirmed FROM users WHERE mem_status='confirmed'";
                          $result1=mysql_query($confirmed) or die(mysql_error());

                               while($row=mysql_fetch_array($result1))
                                {
                                echo "<b>"."Total Confirmed Members: "."</b>".$row['Confirmed'];
                                }
                     ?>
                     <br>
                      <?php
                          $all_mem="SELECT COUNT(user_id) AS members FROM users";
                          $result1=mysql_query($all_mem) or die(mysql_error());

                               while($row=mysql_fetch_array($result1))
                                {
                                echo "<b>"."All Members: "."</b>".$row['members'];
                                }
                     ?>
                     <br>
                <a href="pending_mem.php" class="pending">View Pending Members here</a><br>
                <a href="confirmed_mem.php" class="confirmed">View Confirmed Members here</a>
              
             </div><br><br>

              <form action="" method="post">
                 <div id="input">Search A Member: <input type="text" name="user-id" placeholder="user_id"></div>
                 <input type="submit" name="submit" id="actions" value="Search">
              </form><br>

              <form action="" method="post" id="table_2">
                 <div id="input">Delete A Member: <input type="text" name="user-id" placeholder="user_id"></div>
                 <input type="submit" id="actions" name="del"value="Delete" onclick="return confirm('Are you sure you want to DELETE the user?');">
              </form><br>

              <form action="" method="post" id="table_3">
                 <div id="input">Update Details: <input type="text" name="user-id" placeholder="user_id"></div>
                 <input type="submit" id="actions" value="Update" name="upd">
              </form><br>
              <a href="addition_details.php" class="new_2">Add Member More Details</a> <br>
              <a href="add_user.php" class="new_2"> Or add a new member</a><br>
  <!--function to search and display a member-->
              <?php
              if(isset($_POST['submit']))
              {      
                    $id=$_POST['user-id'];

                       $find="SELECT user_id FROM users WHERE user_id='$id'";

                       $show="SELECT users.*,more_details.*,SUM(amount_paid) AS Balance
                              FROM users 
                              LEFT JOIN more_details
                              ON  users.user_id=more_details.user_id
                              LEFT JOIN payments
                              ON users.user_id=payments.user_id 
                              WHERE users.user_id=$id";

                       $result1=mysql_query($find) or die(mysql_error());
                       $result=mysql_query($show) or die(mysql_error());

                       if(mysql_num_rows($result1)==0)
                       {
                        echo "<script> alert('User Doesnt Exist!! Either Deleted or Yet Added!');</script>";
                       }
                  
                    while($row=mysql_fetch_array($result))
                         {
                          $bal=$sum - $row['Balance'];
                          ?>
                          <div id="result">
                          <span id="welcome1">Showing Details For User Id NO: <?php echo $id;?></span>
                  <img src="<?php echo $row['path'];?>" height="180px" width="200px" class="img-border pad-1" align="right"><br><br>
                     <b>Name:</b> <?php echo $row['fname'];?> <?php echo $row['lname'];?><br>
                     <b>Phone:</b> <?php echo $row['phone'];?> <br>
                     <b>Email:</b> <?php echo $row['email'];?><br>
                     <b>Gender:</b> <?php echo $row['gender'];?><br>
                     <b>Weight:</b> <?php echo $row['weight'];?><br> 
                     <b>Height:</b> <?php echo $row['height'];?><br>
                     <b>BMI status:</b> <?php echo $row['BMI'];?><br>
                     <b>Fee Balance:</b> <?php echo $bal;?><br>
                     <b>Membership Status:</b> <?php echo $row['mem_status'];?><br>
                     <b>Joined On:</b> <?php echo $row['date_joined'];?><br>
                      </div>
                         <?php
                         }

               }
                ?>
                <br><br>
  <!--function to delete a member-->
          <?php
               if(isset($_POST['del']))
               {
      
                $user=$_POST['user-id'];

                  $find="SELECT user_id FROM users WHERE user_id='$user'";
                  $result1=mysql_query($find) or die(mysql_error());

                  $delete="DELETE from users WHERE user_id='$user'";
                  $result = mysql_query($delete);
              if(mysql_num_rows($result1)==0)
                       {
                        echo "<script> alert('User Id Entered Doesnt Exist!!');</script>";
                       }
             else
                 {

                  echo  "<script> alert('User profile Deleted Successfully!');</script>";
                 }
               }
          ?>
<!--function to put new values-->
          <?php
               if(isset($_POST['apt-values']))
               {
      
                $id=$_POST['user-id'];


                  $query="UPDATE users SET fname='$_POST[first]',lname='$_POST[last]',password='$_POST[pass1]',phone='$_POST[phone]',gender='$_POST[gender]' WHERE user_id='$id'";
                  $result = mysql_query($query);
              if(!$query)
                       {
                        echo "Failed to update ".mysql_error();
                       }
             else
                 {

                  echo  "<script> alert('User profile updated Successfully!');</script>";

                  $show="SELECT users.*,more_details.* 
                              FROM users 
                              LEFT JOIN more_details
                              ON  users.user_id=more_details.user_id 
                              WHERE users.user_id=$id";
                  $result=mysql_query($show) or die(mysql_error());
                   while($row=mysql_fetch_array($result))
                     {
                  ?>
                  
                  <div id="result1">
                    <span id="welcome1">New Updated Details For User Id NO: <?php echo $id;?>!</span><br><br>
                   <img src="<?php echo $row['path'];?>" height="180px" width="200px" class="img-border pad-1" align="right"><br><br>
                     <b>Name:</b> <?php echo $row['fname'];?> <?php echo $row['lname'];?><br>
                     <b>Phone:</b> <?php echo $row['phone'];?> <br>
                     <b>Email:</b> <?php echo $row['email'];?><br>
                     <b>Gender:</b> <?php echo $row['gender'];?><br>
                     <b>Weight:</b> <?php echo $row['weight'];?><br> 
                     <b>Height:</b> <?php echo $row['height'];?><br>
                     <b>BMI status:</b> <?php echo $row['BMI'];?><br>
                     <b>Fee Balance:</b> <?php echo $row['Balance'];?><br>
                     <b>Membership Status:</b> <?php echo $row['mem_status'];?><br>
                     <b>Joined On:</b> <?php echo $row['date_joined'];?><br>
                      </div>
                 <?php 
                   }
                 }
               }
          ?>
  <!--function to update user details-->
          <?php
               if(isset($_POST['upd']))
               {
                  $id=$_POST['user-id'];

                       $find="SELECT user_id FROM users WHERE user_id='$id'";
                       $show="SELECT * FROM users WHERE $id=user_id";

                       $result1=mysql_query($find) or die(mysql_error());
                       $result=mysql_query($show) or die(mysql_error());

                       if(mysql_num_rows($result1)==0)
                       {
                        echo "<script> alert('User Doesnt Exist!! Either Deleted or Yet Added!');</script>";
                       }
                  
                    while($row=mysql_fetch_array($result))
                         {
                          ?>

                          <div id="result">
                            <b><u>CURRENT DETAILS</u></b><br><br>
                     <b>User-id:</b> <?php echo $row['user_id'];?><br>
                     <b>Name:</b> <?php echo $row['fname'];?> <?php echo $row['lname'];?><br>
                     <b>Phone:</b> <?php echo $row['phone'];?> <br>
                     <b>Email:</b> <?php echo $row['email'];?><br>
                     <b>Gender:</b> <?php echo $row['gender'];?><br>
                     <b>Joined On:</b> <?php echo $row['date_joined'];?><br>
                     <b>Profile Pic:</b><br><img src="<?php echo $row['path'];?>" height="135px" width="180px" class="img-border pad-1" align=""><br><br>

              <div id="update_new"><b><u>UPDATE WITH: </u></b><br>
                      <table border="0" cellpadding="2" cellspacing="7" width="110%">
                         <form name="register"method="post" action="" enctype="multipart/form-data">
                             <tr><td  class="space">User Id</td><td><div id="inputs"><input type="text" maxlength="16" name="user-id" value=<?php echo $row['user_id'];?> ></div></td></tr>
                             <tr><td  class="space">New First name</td><td><div id="inputs"><input type="text" maxlength="16" name="first" 
                             value= <?php echo $row['fname'];?>  ></div></td></tr>
                             <tr><td class="space">New Surname</td><td><div id="inputs"><input type="text" maxlength="16"  name="last" 
                              value=<?php echo $row['lname'];?> ></div></td></tr>
                             <tr><td class="space">New Password</td><td><div id="inputs"><input type="text" maxlength="60"  name="pass1"
                              value=<?php echo $row['password'];?> ></div></td></tr>
                             <tr><td class="space">Confirm New Password</td><td><div id="inputs"><input type="text"  maxlength="60"  name="pass2"
                               id="pass2" value=<?php echo $row['password'];?> ></div></td></tr>
                             <tr><td class="space">New Phone no.</td><td><div id="inputs"><input type="text" maxlength="15"  name="phone" 
                              value=<?php echo $row['phone'];?> ></div></td></tr>
                             <tr><td class="space">Update Gender</td><td>
                                <div id="inputs"><input type="text" maxlength="15"  name="gender" value=<?php echo $row['gender'];?>  ></div>
                            <tr><td class="space">New Profile Pic</td><td><input type="file" name="file" /></td></tr>
                            <tr><td colspan="2" align="center">
                              <button type="submit" id="actions"name="apt-values">Update</button></td></tr>
                        </form>
                    </table>
            </div>
                     
                </div>
                         <?php
                         }
                    
               }
          ?><br>
            </div>
           </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
   <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
