<?php
require 'connect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Booking</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>

<style type="text/css">
h4{
  color:#01a5ca;
  font-size: 1.3em;
  font-style: italic;
  margin-bottom: 5px;
}
</style>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li class="current"><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1">
              <h2 class="p3"><span class="color-1">Thank you </span> for Requesting!</h2><br>
<?php
      
if ($_POST['fir_name'] != "" || $_POST['email'] != "")
             
      {
    $name=$_POST['email'];

    $querry=mysql_query("INSERT INTO bookings (f_name,l_name,email_add,phone,reason,description,start_date,time_booked,date_booked)
             VALUES ('$_POST[fir_name]','$_POST[sec_name]','$_POST[email]','$_POST[phone]','$_POST[purpose]','$_POST[details]','$_POST[date_from]',NOW(),Now())");
     //catches the errors
     if(!$querry)
        {
           echo "Could not insert ".mysql_error();
        }
            
          echo "<span style='font-size:1.2em;font-family:tahoma'>Thank you for submitting your request.Please wait while we analise the request for convinience.Details will be emailed to you upon approval.</span>";
                  
                   
                  $query="SELECT * FROM bookings WHERE email_add='$name'";
                     $result = mysql_query($query);
                        
                        if(!$result)
                         {
                          echo "Error ".mysql_error();
                         }

        while($row=mysql_fetch_array($result)){

                    $f_name=$row['f_name'];
                    $l_name=$row['l_name'];
                    $email=$row['email_add'];
                    $reason=$row['reason'];
                    $start=$row['start_date'];
                    $book=$row['time_booked'];
                    $date=$row['date_booked']

          //send email to the client on booking the appointment


                   /*  $to = $email;
                     $subject = "The Booking Request";
                     $message = "Hello $f_name \n, Your booking request has been received.\nDetails of Application.\n
                                         Name: $f_name
                                         Purpose: $reason
                                         Booked Date: $start
                                         Booked on $date At $book
                                         
                                         \r\n.This is automatically sent Email.Please do not reply.";
                    $header="Admin sam_ndi gymnasiums";
                  
                   //mail($to,$subject,$message,$header);*/
                
          ?>
          <br><br>
          <h4>Details of your Request</h4>
             
          <table width="80%" name="booking_details" border="0px" cellspacing="5" cellpadding="2"> 
            <tr><td><b>Name:</b></td><td><?php echo $f_name;?> <?php echo $l_name;?></td></tr>
            <tr><td><b>Email Address:</b></td><td><?php echo $email;?></td></tr>
            <tr><td><b>Purpose:</b></td><td><?php echo $reason;?></td></tr>
            <tr><td><b>Booked Date:</b></td><td><?php echo $start;?></td></tr>
            <tr><td><b>Time Booked:</b></td><td><?php echo $book?> On <?php echo $date?></td></tr>
          </table>

       <p class="join">Join us today at <a href="register.php">Sam_Ndi Gymnasium</a> For Best Training Services.</p>
          <?php
       }
     }
   else
   {
    echo "Fill in the required Details!!";
    $redirect = $_SERVER['PHP_SELF'];

                header("Refresh: 5; URL=book.php?redirect=$redirect");

   }   
 ?>
             
            </div>
            <div class="col-2">
            <h2 class="p6"><span class="color-1">Services</span> Offered</h2><br>
              <img src="images/page4-img2.jpg" alt="" class="img-border">
              <p class="p2 top-6"><strong>List of our Services</strong></p>
              <ul class="list-2">
                <li>Main work our area</li>
                <li>Cardio area/theatre</li>
                <li>Group exercise classes</li>
                <li>Sports facilities</li>
                <li>Child care facilities</li>
              </ul>
             </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
