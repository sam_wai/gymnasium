<?php
session_start();
require 'connect.php';
//require 'authenticate.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Register</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Services</a>
           <ul><li class="current"><a href="register.php">Become a Member</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1">
              <h2 class="p3"><span class="color-1">Register and </span> Become a Member</h2><br><br>
             <?php
        if(isset($_POST['email']) && isset($_POST['password']))
  
      {
         $email =$_POST['email'];
         $password = $_POST['password'];
    
        if(!empty($password) && !empty($email))
       {
                  $query = "SELECT `email`,`password` FROM `users` WHERE `email` = '$email' AND `password` = '$password'";
                  $check_query  = mysql_query($query);

            if($check_query)
              {
                       $num_rows_returned  = mysql_num_rows($check_query);
                        
                        if($num_rows_returned == 0)
                     {
                        //header('Location: register.php');
                        echo '<div id="alert_login">Wrong email address or password !! Try again or Register if new here.</div>';
                        echo "<br>";
                        ?>
                          <div id="table">
                                        <table  border="0" cellpadding="2" cellspacing="5" width="110%"  align="">
                                          <form name="register"method="post" action="signup.php">
                                            <tr><td class="space">First name</td><td><div id="inputs"><input type="text" required="required" maxlength="16" name="first" placeholder=""/></div></td></tr>
                                            <tr><td class="space">Surname</td><td><div id="inputs"><input type="text" required="required" maxlength="16"  name="last" /></div></td></tr>
                                            <tr><td class="space"> Email</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email"/></div></td></tr>
                                            <tr><td class="space">Confirm Email</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email2"/></div></td></tr>
                                            <tr><td class="space">Phone no.</td><td><div id="inputs"><input type="text" required="required" maxlength="15"  name="phone" /></div></td></tr>
                                            <tr><td class="space">Gender</td><td>
                                                                   <select name="gender"> 
                                                                       <option selected="selected">Choose</option> 
                                                                       <option>Male</option> 
                                                                       <option>Female</option>
                                                                   </select>
                                            </td></tr>
                                            <tr><td class="space">Profile Picture</td><td><input type="file" name="file" /></div></td></tr>
                                            <tr><td colspan="2" align="center"><br />
                                            <button type="submit" id="actions">Sign Up</button></td></tr>
                                          </form>
                                        </table>
                                    </div>
                                     ';
               <?php                
                   }
      elseif($num_rows_returned == 1)
      {
        $_SESSION['logged_user'] = $_POST['email']; 
        $_SESSION['user_password'] = $_POST['password'];

        $name=$_POST['email']; 

                
                $query="SELECT fname,lname FROM users WHERE email='$name'";
                $result = mysql_query($query);
                if(!$result)
                    {
                          echo "Error ".mysql_error();
                    }
                else{

                
                  while($row=mysql_fetch_array($result)){
                    $fname=$row['fname'];
                    $lname=$row['lname'];
                    if($fname=='ADMIN')
                    {
                      header('location:admin_menu.php');
                    }

                    else 
                    header('location:profile.php');

                  }
              }
                    
                        
          
        }

      }
    else
      {
        echo 'query failed.'.mysql_error();
      }

        }
        else
         {
        echo '<p style="font-family:tahoma;font-size:1.2em;">ERROR: You must provide email and Password in order to loggin or <a href="register.php">signup</a> if you dont have an account already</style></p>';
        }
}

?>
            </div>
            <div class="col-2">
              <h2 class="p6"><span class="color-1">Member</span> Login</h2><br><br>
                 <form action="log_in.php" method="post" name="logginForm">
                       <div id="inputs">
                         <label for="email">Email
                         <input id="username" type="email" name="email" value="<?php echo $_POST['email'] ;?>" required></label><br><br>
                         <label for="password">Password            
                         <br><input id="password" type="password" name="password" placeholder="Password" required></label>
                      </div>
    
                     
                         <button type="submit" id="actions">Log in</button><br>
                         <label><input class="check" type="checkbox" checked="checked"> Keep me signed in</label>
                             <br><br>
                   
                </form>
               </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
