<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Contacts</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li class="current"><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-3">
              <h2><span class="color-1">Find</span> us </h2>

              <div class="map img-border">
                <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=University+of+Nairobi+Graduation+Square+Grounds,+Nairobi,+Kenya&amp;aq=0&amp;oq=University+of+Nairobi&amp;sll=-1.271588,36.80375&amp;sspn=0.022697,0.042272&amp;ie=UTF8&amp;hq=University+of+Nairobi+Graduation+Square+Grounds,&amp;hnear=Nairobi,+Nairobi+Province,+Kenya&amp;ll=-1.277734,36.81241&amp;spn=0.03153,0.02098&amp;t=m&amp;output=embed"></iframe><br /><small>View <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=University+of+Nairobi+Graduation+Square+Grounds,+Nairobi,+Kenya&amp;aq=0&amp;oq=University+of+Nairobi&amp;sll=-1.271588,36.80375&amp;sspn=0.022697,0.042272&amp;ie=UTF8&amp;hq=University+of+Nairobi+Graduation+Square+Grounds,&amp;hnear=Nairobi,+Nairobi+Province,+Kenya&amp;ll=-1.277734,36.81241&amp;spn=0.03153,0.02098&amp;t=m" style="color:#0000FF;text-align:left" target="blank">University of Nairobi</a> in a larger map</small>
              </div>
              <br><br>
              <dl>
                <dt class="color-1"><strong>Chiromo Road,<br>
                  University of Nairobi</strong></dt>
                <dd><span>Cellphone:</span>+254 715 683805</dd>
                <dd><span>Telephone:</span>020 272 1417</dd>
                <dd><span>E-mail:</span><a href="mailto:s.ndiba@yahoo.com" class="link">s.ndiba@yahoo.com</a></dd>
              </dl>
            </div>
            <div class="col-4">
              <h2><span class="color-1">Contact</span> form</h2>
              <p class="contacts">If you would like to communicate to us,please fill in the form with your message or querry and send it to us.Youcan still visit us following the map provided.We really apprecite your feedback and querry on our services.Thank you.</p>
              <form id="form" method="post" action="#">
                <p>
                      <input type="hidden" name="receipient" value="s.ndiba@yahoo.com">
                      <input type="hidden" name="Query" value="Contact Form">
                      <input type="hidden" name="redirect" value="index.php">
                </p>
                <fieldset>
                  <label>
                    <input type="text" value="Name" onBlur="if(this.value=='') this.value='Name'" onFocus="if(this.value =='Name' ) this.value=''">
                  </label>
                  <label>
                    <input type="text" value="Email" onBlur="if(this.value=='') this.value='Email'" onFocus="if(this.value =='Email' ) this.value=''">
                  </label>
                  <label>
                    <textarea onBlur="if(this.value==''){this.value='Message'}" onFocus="if(this.value=='Message'){this.value=''}">Message</textarea>
                  </label>
                  <div class="btns"><button type="submit" class="button" name="submit">Send</button></div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
  <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
