<?php
//includes the file that connects and selects the database
require 'connect.php';

session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Staff</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>

<script type="text/javascript">
function startTime()
{

   var today=new Date();
   var h=today.getHours();
   var m=today.getMinutes();
   var s=today.getSeconds();
   
//add zero infront of numbers <10
   m=checkTime(m);
   s=checkTime(s);

       document.getElementById('txt').innerHTML=h+":" +m+":"+s; //formats the display
   
   t=setTimeout('startTime()',500);

function checkTime(i)
  {
   if(i<10)
     {
       i="0"+i
     }
return i;
  }
}
</script>
<script type="text/javascript">
//--------------- LOCALIZEABLE GLOBALS ---------------
var d=new Date();
var monthname=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
//Ensure correct for language. English is "January 1, 2004"
var TODAY = monthname[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body onload= "startTime()">
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
   <div id="admin_head">Sam_Ndi Gymnasium System</div>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
          <div class="wrap block-2">
            <div class="col-1_profile">

         <div class="menu_border">
            <span class="dateformat"> 
              <script language="JavaScript" type="text/javascript">
                    document.write('<i><b>Today </b></i>');  
                    document.write(TODAY);
              </script>
              <i><b>Time</b></i><span id="txt"></span>
            </span>

           <ul class="menu_admin">
               <li><a href="admin_menu.php">Home</a></li>
               <li><a href="users.php">Members</a></li>
               <li><a href="view_bookings.php">Bookings</a></li>
               <li><a href="inventory.php">Equipment</a></li>
               <li><a href="payments.php">Payments</a></li>
               <li class="current"><a href="staff.php">Staff</a></li>
               <li><a href="adminforum.php">Forum</a></li>
           </ul> <br><br>
           
              <form action="admin_menu.php" method="post">
                 <input type="submit" id="logout_admin" value="Main Menu">
              </form><br><br><br><br>

              <form action="" method="post">
                 <div id="input">Display A Staff: <input type="text" name="staff-id" placeholder="staff_no"></div>
                 <input type="submit" name="submit" id="actions" value="Search">
              </form><br>

              <form action="" method="post" id="table_2">
                 <div id="input">Remove A Staff: <input type="text" name="staff-id" placeholder="staff_no"></div>
                 <input type="submit" id="actions" name="del"value="Delete" onclick="return confirm('Are you sure you want to DELETE the user?');">
              </form><br>

              <form action="" method="post" id="table_3">
                 <div id="input">Update Staff Details: <input type="text" name="staff-id" placeholder="staff_no"></div>
                 <input type="submit" id="actions" value="Update" name="upd">
              </form><br>
              <a href="add_staff.php" id="new">Or add a new staff here</a><br>
  <!--function to search and display a member-->
              <?php
              if(isset($_POST['submit']))
              {      
                    $id=$_POST['staff-id'];

                       $find="SELECT staff_no FROM staff WHERE staff_no='$id'";
                       $show="SELECT * FROM staff WHERE staff_no=$id";

                       $result1=mysql_query($find) or die(mysql_error());
                       $result=mysql_query($show) or die(mysql_error());

                       if(mysql_num_rows($result1)==0)
                       {
                        echo "<script> alert('Staff id Doesnt Exist!!Please Confirm!');</script>";
                       }
                  
                    while($row=mysql_fetch_array($result))
                         {
                          ?>
                          <div id="result">
                          <span id="welcome1">Showing Details For Staff Id NO: <?php echo $id;?></span>
                  <img src="<?php echo $row['path'];?>" height="180px" width="200px" class="img-border pad-1" align="right"><br><br>
                     <b>Staff no.: <?php echo $row['staff_no'];?></b> <br>
                     <b>Full Name:</b> <?php echo $row['first_name'];?> <?php echo $row['sec_name'];?><br>
                     <b>Phone No.:</b> <?php echo $row['phone_number'];?> <br>
                     <b>Email:</b> <?php echo $row['email_address'];?><br>
                     <b>Role:</b> <?php echo $row['role'];?><br> 
                      </div>
                         <?php
                         }

               }
                ?>
                <br><br>
  <!--function to delete a member-->
          <?php
               if(isset($_POST['del']))
               {
      
                $user=$_POST['staff-id'];

                  $find="SELECT staff_no FROM staff WHERE staff_no='$user'";
                  $result1=mysql_query($find) or die(mysql_error());

                  $delete="DELETE from staff WHERE staff_no='$user'";
                  $result = mysql_query($delete);
              if(mysql_num_rows($result1)==0)
                       {
                        echo "<script> alert('User Id Entered Doesnt Exist!!');</script>";
                       }
             else
                 {

                  echo  "<script> alert('User profile Deleted Successfully!');</script>";
                 }
               }
          ?>
<!--function to put new values-->
          <?php
               if(isset($_POST['apt-values']))
               {
      
                $id=$_POST['staff-id'];


                  $query="UPDATE staff SET first_name='$_POST[first]',sec_name='$_POST[last]',email_address='$_POST[email1]',phone_number='$_POST[phone]',role='$_POST[role]' WHERE staff_no='$id'";
                  $result = mysql_query($query);
              if(!$query)
                       {
                        echo "Failed to update ".mysql_error();
                       }
             else
                 {

                  echo  "<script> alert('Staff details updated Successfully!');</script>";

                  $show="SELECT * FROM staff WHERE $id=staff_no";
                  $result=mysql_query($show) or die(mysql_error());
                   while($row=mysql_fetch_array($result))
                     {
                  ?>
                  
                  <div id="result1">
                    <span id="welcome1">New Updated Details For User Id NO:<?php echo $row['staff_no'];?>!</span><br>
                   <img src="<?php echo $row['path'];?>" height="150px" width="140px" class="img-border pad-1" align="right"><br><br>
                     <b>Staff no.: <?php echo $row['staff_no'];?></b> <br>
                     <b>Full Name:</b> <?php echo $row['first_name'];?> <?php echo $row['sec_name'];?><br>
                     <b>Phone No.:</b> <?php echo $row['phone_number'];?> <br>
                     <b>Email Address:</b> <?php echo $row['email_address'];?><br>
                     <b>Role:</b> <?php echo $row['role'];?><br> 
                      </div>
                 <?php 
                   }

                 }
               }
          ?>
  <!--function to update user details-->
          <?php
               if(isset($_POST['upd']))
               {
                  $id=$_POST['staff-id'];

                       $find="SELECT staff_no FROM staff WHERE staff_no='$id'";
                       $show="SELECT * FROM staff WHERE $id=staff_no";

                       $result1=mysql_query($find) or die(mysql_error());
                       $result=mysql_query($show) or die(mysql_error());

                       if(mysql_num_rows($result1)==0)
                       {
                        echo "<script> alert('staff_no Doesnt Exist!! Please Confirm!');</script>";
                       }
                  
                    while($row=mysql_fetch_array($result))
                         {
                          ?>

                          <div id="result">
                            <b><u>CURRENT DETAILS</u></b><br><br>
                     <b>staff-id:</b> <?php echo $row['staff_no'];?><br>
                     <b>Name:</b> <?php echo $row['first_name'];?> <?php echo $row['sec_name'];?><br>
                     <b>Phone No.</b> <?php echo $row['phone_number'];?> <br>
                     <b>Email:</b> <?php echo $row['email_address'];?><br>
                     <b>Role.:</b> <?php echo $row['role'];?><br>
                     <b>Profile Pic:</b><br><img src="<?php echo $row['path'];?>" height="135px" width="180px" class="img-border pad-1" align=""><br><br>

                     <div id="update_new"><b><u>UPDATE WITH: </u></b><br>
                      <table border="0" cellpadding="2" cellspacing="7" width="110%">
                         <form name="register"method="post" action="" enctype="multipart/form-data">
                             <tr><td  class="space">User Id</td><td><div id="inputs"><input type="text" maxlength="16" name="staff-id" value=<?php echo $row['staff_no'];?> ></div></td></tr>
                             <tr><td  class="space">New First name</td><td><div id="inputs"><input type="text" maxlength="16" name="first" 
                             value= <?php echo $row['first_name'];?>  ></div></td></tr>
                             <tr><td class="space">New Surname</td><td><div id="inputs"><input type="text" maxlength="16"  name="last" 
                              value=<?php echo $row['sec_name'];?> ></div></td></tr>
                             <tr><td class="space">New Email</td><td><div id="inputs"><input type="text" maxlength="60"  name="email1"
                              value=<?php echo $row['email_address'];?> ></div></td></tr>
                             <tr><td class="space">Confirm New Email</td><td><div id="inputs"><input type="text"  maxlength="60"  name="email2"
                               id="pass2" value=<?php echo $row['email_address'];?> ></div></td></tr>
                             <tr><td class="space">New phone_number no.</td><td><div id="inputs"><input type="text" maxlength="15"  name="phone" 
                              value=<?php echo $row['phone_number'];?> ></div></td></tr>
                              <tr><td class="space">New Role.</td><td><div id="inputs"><input type="text" maxlength="15"  name="role" 
                              value=<?php echo $row['role'];?> ></div></td></tr>
                            <tr><td class="space">New Profile Pic</td><td><input type="file" name="file" /></td></tr>
                            <tr><td colspan="2" align="center">
                              <button type="submit" id="actions"name="apt-values">Update</button></td></tr>
                        </form>
                    </table>
                    </div>
                     
                </div>
                         <?php
                         }
                    
               }
          ?><br>

  <!--end of functions || begin of the body section-->

      <span id="welcome1"><u>All The Staff Members of The Sam_Ndi Gym Listed:</u></span><br><br>
              
  <!--function to display all the staff from the database-->

              <?php

                  $query="SELECT * FROM staff";
                     $result = mysql_query($query);
                        if(!$result)
                         {
                          echo "Error ".mysql_error();
                         }

                       else{

                     while($row=mysql_fetch_array($result))
                     {
                  ?>
                  <div id="result">
                   <img src="<?php echo $row['path'];?>" height="180px" width="200px" class="img-border pad-1" align=""><br><br>
                     <b>Staff no.: <?php echo $row['staff_no'];?></b> <br>
                     <b>Full Name:</b> <?php echo $row['first_name'];?> <?php echo $row['sec_name'];?><br>
                     <b>Phone No.:</b> <?php echo $row['phone_number'];?> <br>
                     <b>Email Address:</b> <?php echo $row['email_address'];?><br>
                     <b>Role:</b> <?php echo $row['role'];?><br> 
                      </div>
              <?php 
             }
           }
        ?>
  <!--end of the function || end of the body section-->
              
            </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
   <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
