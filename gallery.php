<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Gallery</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/demo.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/uCarousel.js"></script>
<script src="js/tms-0.4.1.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<script>
$(document)
    .ready(function () {
    $('.gallery')
        ._TMS({
        show: 0,
        pauseOnHover: true,
        prevBu: '.prev',
        nextBu: '.next',
        playBu: '.play',
        duration: 700,
        preset: 'fade',
        pagination: $('.img-pags')
            .uCarousel({
            show: 4,
            shift: 0
        }),
        pagNums: false,
        slideshow: 7000,
        numStatus: true,
        banners: false,
        waitBannerAnimation: false,
        progressBar: false
    })
})
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li class="current"><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div id="slide">
          <div class="gallery">
            <ul class="items">
              <li><img src="view/photos/photo1.jpg" alt=""></li>
              <li><img src="view/photos/photo2.jpg" alt=""></li>
              <li><img src="view/photos/photo3.jpg" alt=""></li>
              <li><img src="view/photos/photo4.jpg" alt=""></li>
              <li><img src="view/photos/photo5.jpg" alt=""></li>
              <li><img src="view/photos/photo6.jpg" alt=""></li>
              <li><img src="view/photos/photo7.jpg" alt=""></li>
              <li><img src="view/photos/photo8.jpg" alt=""></li>
            </ul>
          </div>
          <a href="#" class="prev"></a><a href="#" class="next"></a> </div>
      </div>
      <div class="grid_12 top-1">
        <div class="block-3 box-shadow">
          <h2 class="p4"><span class="color-1">Photo</span> gallery</h2>
          <div class="pag">
            <div class="img-pags">
              <ul>
                <li><a href="#"><span><img src="files/photos/photo1.jpg" alt=""></span></a></li>
                <li><a href="#"><span><img src="files/photos/photo2.jpg" alt=""></span></a></li>
                <li><a href="#"><span><img src="files/photos/photo3.jpg" alt=""></span></a></li>
                <li><a href="#"><span><img src="files/photos/photo4.jpg" alt=""></span></a></li>
                <li><a href="#"><span><img src="files/photos/photo5.jpg" alt=""></span></a></li>
                <li><a href="#"><span><img src="files/photos/photo6.jpg" alt=""></span></a></li>
                <li><a href="#"><span><img src="files/photos/photo7.jpg" alt=""></span></a></li>
                <li><a href="#"><span><img src="files/photos/photo8.jpg" alt=""></span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
