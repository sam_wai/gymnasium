<?php
//includes the file that connects and selects the database
session_start();
require 'connect.php';
require 'authenticate.php';
$sum=30000;

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Profile</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<link href="css/htmlDatePicker.css" rel="stylesheet">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<script src="js/htmlDatePicker.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li  class="current"><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1_profile">
              <h2 class="p3"><span class="color-1">View </span> My Profile</h2><br>
              <?php 
                $user=$_SESSION['logged_user'];
                $query="SELECT fname FROM users WHERE email='$user'";
                $result = mysql_query($query);
                if(!$result)
                    {
                          echo "Error ".mysql_error();
                    }

                else{

                
                  while($row=mysql_fetch_array($result)){
                    $fname=$row['fname'];
                      echo "<div id='welcome'>Welcome "."<u><i>".$fname."</i></u>"." to your profile! </div>"."<br>";?>
             
             <form action="log_out.php" method="post">
                 <input type="submit" id="logout" value="Log Out">
              </form> 
             
    <?php
                  }
              }
    ?>
            
            <?php
                 $user=$_SESSION['logged_user'];

                  $query="SELECT users.*,users.user_id AS userid,more_details.*,SUM(amount_paid) AS Balance
                          FROM users 
                          LEFT JOIN more_details 
                          ON  users.user_id=more_details.user_id
                          LEFT JOIN payments
                          ON users.user_id=payments.user_id
                          WHERE email='$user'";
                     $result = mysql_query($query);
                        if(!$result)
                         {
                          echo "Error ".mysql_error();
                         }

                      else{

                     while($row=mysql_fetch_array($result))
                     {

                       $userid=$row['userid'];

                       //echo $userid;

                    $f_name=$row['fname'];
                    $l_name=$row['lname'];
                    $phone=$row['phone'];
                    $email=$row['email'];
                    $gender=$row['gender'];
                    $date=$row['date_joined'];
                    $weight=$row['weight'];
                    $height=$row['height'];
                    $BMI=$row['BMI'];
                    $bal=$sum - $row['Balance'];
                    $status=$row['mem_status'];

             if (mysql_num_rows($result) != 0)
             {
                if ($status =='Cancelled') 
                {
                  echo "<div class='cancel'>Hello ".$f_name.", We are sorry your Membership request has been Cancelled.Please <a href='contacts.php'>contact</a> us to get full Details of cancellation.</div>";
                }
                else
                {
               ?>
               <span id="tagline">"Where we care about your heath and your future" </span><br><br><br><br>
          
              <img src="<?php echo $row['path'];?>" height="180px" width="170px" class="circleborder" align=""><br><br>
              <span class="general">GENERAL DETAILS</span><br><br>
                <table width="40%" name="profile_details" border="0px" cellspacing="5" cellpadding="2"> 
                    <tr><td class="format_prof">Full Name:</td><td><span style="font-style:italic;"><?php echo $f_name;?> <?php echo $l_name;?></span></td></tr>
                    <tr><td class="format_prof">Email Address:</td><td><span style="font-style:italic;"><?php echo $email;?></span>
                    <tr><td class="format_prof">Cell Phone:</td><td><span style="font-style:italic;"><?php echo $phone;?></span></td></tr>
                    <tr><td class="format_prof">Gender: </td><td><span style="font-style:italic;"><?php echo $gender;?></span></td></tr>
                    <tr><td class="format_prof">Joined Date</td><td><span style="font-style:italic;"><?php echo $date;?></span></td></tr>
                    
                </table>

                <div class="personal"> 
                  <span style="font-weight:bold;font-size:1.2em;"><u>PERSONAL DETAILS</u></span><br><br>

                  <span style="font-weight:bold">Membership status: </span><span style="font-style:italic;"><?php echo $status;?></span><br>
                  <span style="font-weight:bold">Weight: </span><span style="font-style:italic;"><?php echo $weight;?></span><br>
                  <span style="font-weight:bold">Height: </span><span style="font-style:italic;"><?php echo $height;?></span><br>
                  <span style="font-weight:bold">BMI Current Status: </span><span style="font-style:italic;"><?php echo $BMI;?></span><br>
                  <span style="font-weight:bold">Fee Balance: </span><span style="font-style:italic;">Ksh. <?php echo $bal;?></span><br>
                </div>
              <?php 
                 
               
               echo "<div class='up_details'><a href='update_det.php?read=$userid'>Update Details</div></a>";
               echo "<br>";
             }
             ?>
      <iframe style="margin-top:30px;margin-left:5px"src="http://abiliti.com/en/bmicalc?metric=true" width="560" height="300" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
             <?php
           }
           }
         }
         
        ?>
            </div>
        
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
