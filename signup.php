<?php
//includes the file that connects and selects the database
require 'connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Register</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Services</a>
           <ul><li class="current"><a href="register.php">Become a Member</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1_signup">
              <h2 class="p3"><span class="color-1">Register and </span> Become a Member</h2><br><br>
<?php 
//if(isset($_POST['submit']))
 //Two variable to hold entered email and phone number
 $ema=$_POST['email'];
 $fon=$_POST['phone'];

 $querry="SELECT email FROM users WHERE email='$ema'";
 $querry1="SELECT phone FROM users WHERE phone='$fon'";
//variable to store the search results from the database
 $result=mysql_query($querry) or die(mysql_error());
 $result1=mysql_query($querry1) or die(mysql_error());
 
//if an emails that corresponds to the one entered,it alerts the user and displays the registration table to be filled again

if (mysql_num_rows($result) != 0)
{
echo "<div id='alert'>Email "."<span id='warn'>".$ema."</span>"." exists!!</div>"."<br>" ;

?>
       <div id="table">
         <table  border="0" cellpadding="2" cellspacing="5" width="100%">
           <form name="register"method="post" action="signup.php">
             <tr><td class="space">First name</td><td><div id="inputs"><input type="text" required="required" maxlength="16" name="first" value="<?php echo $_POST['first'] ;?>"/></div></td></tr>
             <tr><td class="space">Surname</td><td><div id="inputs"><input type="text" required="required" maxlength="16"  name="last" value="<?php echo $_POST['last'] ;?>"/></div></td></tr>
             <tr><td class="space">Email</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email" value=""/></div></td></tr>
             <tr><td class="space">Confirm Email</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email2" value=""/></div></td></tr>
             <tr><td class="space">Phone no.</td><td><div id="inputs"><input type="text" required="required" maxlength="15"  name="phone" value="<?php echo $_POST['phone'] ;?>"/></div></td></tr>
             <tr><td class="space">Gender</td><td>
                          <select name="gender"> 
                          <option selected="selected">Choose</option> 
                          <option>Male</option> 
                          <option>Female</option>
                          </select>
             </td></tr>
             <tr><td class="space">Profile Picture</td><td><input type="file" name="file" /></div></td></tr>
             <tr><td colspan="2" align="center"><br />
            <button type="submit" id="actions">Sign Up</button></td></tr>
           </form>
         </table>
         </div>
<?php       
}

//if a phone number that corresponds to the one entered,it alerts the user and displays the registration table to be filled again
else if(mysql_num_rows($result1) != 0)
{
    echo "<div id='alert'>The phone number " ."<span id='warn'>".$fon ."</span>". " exists!!</div>"."<br>";

    ?>
       <div id="table">
         <table  border="0" cellpadding="2" cellspacing="5" width="100%">
           <form name="register"method="post" action="signup.php">
             <tr><td class="space">First name</td><td><div id="inputs"><input type="text" required="required" maxlength="16" name="first" value="<?php echo $_POST['first'] ;?>"/></div></td></tr>
             <tr><td class="space">Surname</td><td><div id="inputs"><input type="text" required="required" maxlength="16"  name="last" value="<?php echo $_POST['last'] ;?>" /></div></td></tr>
             <tr><td class="space">Email</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email" value="<?php echo $_POST['email'] ;?>"/></div></td></tr>
             <tr><td class="space">Confirm Email</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email2" value="<?php echo $_POST['email2'] ;?>"/></div></td></tr>
             <tr><td class="space">Phone no.</td><td><div id="inputs"><input type="text" required="required" maxlength="15"  name="phone" value=""/></div></td></tr>
             <tr><td class="space">Gender</td><td>
                          <select name="gender"> 
                             <option selected="selected">Choose</option> 
                             <option>Male</option> 
                             <option>Female</option>
                          </select>
             </td></tr>
             <tr><td class="space">Profile Picture</td><td><input type="file" name="file" /></div></td></tr>
             <tr><td colspan="2" align="center"><br />
            <button type="submit" id="actions">Sign Up</button></td></tr>
           </form>
         </table>
        </div>
 <?php      
}

//if all is well stores the entered data in the database
else 
{
    function randomPass($length)
     {
        $password="";
        $length=$length;
        $chars="abcdefghijklmnopqrstuvwxyz123456789";

        for($x=1;$x<=$length;$x++)
           
         {
                $rand=rand() %strlen($chars);
                $temp=substr($chars,$rand,1);
                $password.=$temp;
            }
    return $password;
    }
  
  
      $pass=randomPass(7);

      $pas1=md5($pass);

      
              $to = $_POST['email'];
              $user=$_POST['first'];
              $subject = "The Loggin Password";
              $message = "Hello $user,\n The Password is $pass .Use it to loggin to our site.\n\nThis is automatically sent Email.Please do not reply.";
              $header="Admin sam_ndi gymnasiums";
                  //mail($to,$subject,$message,$header);

 //$loc="upload/";
   
   if($_FILES['file']["type"]=="image/png"||$_FILES['file']["type"]=="image/jpeg"||$_FILES['file']["type"]=="image/jpg"||$_FILES['file']["type"]=="image/gif"
       ||$_FILES['file']["type"]=="image/pdf"||$_FILES['file']["type"]=="image/docx")
   { 
   
   
   $name=mysql_real_escape_string(basename($_FILES['file']['name']));
   $t_name=$_FILES['file']['tmp_name'];
   $dir='upload';
   //$body=$_POST['body']; 
      if(move_uploaded_file($t_name,$dir."/".$name)){
      
   }
     else{
        echo 'Wrong image format!';
    } 
     $querry=mysql_query("INSERT INTO users (fname,lname,email,phone,password,gender,date_joined,image_name,path)
             VALUES ('$_POST[first]','$_POST[last]','$_POST[email]','$_POST[phone]','$pass','$_POST[gender]',NOW(),'$name','upload/$name')");
     //catches the errors
    if(!$querry)
        {
           echo "Could not insert ".mysql_error();
           echo"Invalid image format!!";

        }



        else
          {
          
          echo "<div class='welcomName'>";
          echo "Welcome ".$_POST['first'] ."&nbsp;".$_POST['last'].",";
          echo "</div>";
          echo "<br>";
          echo "Wait as we redirect you to our loggin page.Please use the <span style='text-decoration:underline;font-style:italic;'>password sent to your email address</span> to loggin.If you are not redirected automatically,click <a href='register.php'>here</a> to loggin.";
     
            $redirect = $_SERVER['PHP_SELF'];
            header("Refresh: 5; URL=register.php?redirect=$redirect");
     }  
    }
   } 
 
   ?>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
