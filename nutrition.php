<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Nutrition</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li class="current"><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1">

              <h2 class="p3"><span class="color-1">We do care</span> Home your health</h2>
              <p class="head_up">Best Food For Your Exercise Performance And Lose Weight <span class="doct">by Dr Johnstone Mwangi and our trainer team</span></p>    
                <div class="wrap box-1 top-4"> <img src="images/nutrition-img1.jpg" alt="" class="img-border img-indent"></div>
                <br>Whether your goal is to lose weight or improve your exercise performance, the right foods can help fuel your body in the right way. The key is to start with a general healthy diet.
                 <br><br>"The best foods are wholegrain breads and cereals, fruits and vegetables, lean meats and low-fat dairy foods," says our dietitian Dr Johnstone Mwangi. "If you're including all those in your daily diet as a baseline, you'll  be well on the way to performing well and getting your best results."            
                 <br><br>An hour after exercising, opt for food with carbohydrates and protein, such as grainy toast and ham. Watson says this will "replenish the glycogen you've burned and repair the muscles you've disturbed during exercise".
                 <br><br>A few easy diet adjustments on your workout days can also balance energy levels and help recovery.</p>
                 <p style="font-size:1.4em;color:#01a5ca;margin-top:10px;font-style:italic;font-family:script mj">Know Your BMI Today</p>
               <iframe style="margin-top:30px;margin-left:5px"src="http://abiliti.com/en/bmicalc?metric=true" width="560" height="300" frameborder="0" marginwidth="0" marginheight="0"></iframe> <br><br>
              <div class="wrap box-1"> 
                <div class="extra-wrap">
                  <p class="p2_nutrition">Foods for cardio</p>
                  If weight loss is your goal, it's best to do aerobic exercise in a fasted state when you first wake up. "That means you're using fat for your energy fuel," says sports nutritionist and trainer Ruth Kariuki.
                    <br><br>But if your goal is to improve your time or distance, a small carbohydrate-rich snack, such as fruit, in the hour before you train will give your body glycogen, which is a more powerful energy source
                    <br><br>Iron deficiency is a risk for female long-distance runners, so their diet needs plenty of red meat, chicken, fish, eggs, wholegrains and legumes to keep iron levels up.
                </div>
              </div>

              <div class="wrap box-1"> 
                <div class="extra-wrap">
                  <p class="p2_nutrition">Foods for endurance</p>
                    Long-distance athletes need to maintain carbohydrate intake throughout the session. "Hitting the wall? That's the point where you've run out of carbohydrate stores," Mwangi says.
                     <br><br>Experts suggest consuming one gram of carbohydrate per kilogram of body weight per hour. So if you weigh 60 kilograms, you'll need 60 grams per hour. "If you filled a 750ml bottle with a sports drink that's eight per cent glucose solution, you get about 60 grams of carbohydrate," Mwangi says.
                     <br><br>If you're doing a long session on a hot day, such as a tennis match in the sun, Manche suggests drinking two ice-cold bottles of water in the 90 minutes before you exercise. "It lowers your core temperature and we think one of the elements of fatigue is an increase in core temperature," she says. <br><br>"Pre-cooling lowers core temperature, so it takes longer to reach that maximum level and you can keep going for longer."
                   </div>
              </div>

              <div class="wrap box-1"> 
                <div class="extra-wrap">
                  <p class="p2_nutrition">Foods for yoga</p>
                 When it comes to yoga, avoid foods that will feel uncomfortable in your stomach when you're twisting and doing core work. "It's best to not have eaten a meal for two to three hours before a class," says yoga teacher Wanyama Cleophas, our director of Adore Yoga .
                   <br><br>Try to eat healthy, unprocessed foods on days when you've got a class. "If your body and mind are dull because all your energy is being used up digesting heavy or processed foods, it's hard to achieve the clarity and balance that is the goal of yoga," Cleophas says.
                   <br><br>"For a meditative class, the kind of food you want is calming and cooling, so that would be a lot of fresh, lightly cooked vegetables. Avoid things like animal protein. But if you were doing a strong physical practice, you're likely to require a bit more protein because you're building that muscle mass and you need carbs for energy."
                </div>
              </div>

              <div class="wrap box-1"> 
                <div class="extra-wrap">
                  <p class="p2_nutrition">Foods for weights and pilates</p>
                  When preparing for such exercises, you need to balance protein and carbohydrates. "Carbohydrates move the muscle, [while] protein provides the structure," Mwangi says. "To move it, you've got to give it fuel."
                   <br><br>If you're fatigued during or after a workout, you might need a protein and carbohydrate snack, such as berries and yoghurt. These are best eaten an hour beforehand. "Often people find it's easier to eat before a weights session as opposed to cardio because there's not that jumping element," Mwangi says.
                   </div>
              </div>
              
            </div>
            <div class="col-2">
              <h2 class="p6"><span class="color-1">Home</span> nutrition</h2>
              <img src="images/food.jpg" alt="" class="img-border" height="180px" width="200px">
              <p class="p2 top-6"><strong>14 2014 Health Tips For You!</strong></p>
              <p class="p4">Dedicate yourself to a healthy lifestyle in 2014 with these food,nutrition and exercise tips:</p>
              <ul class="list-2">
               <li>Eat breakfast</li>
               <li>Make Half Your Plate Fruits and Vegetables</li>
               <li>Watch Portion Sizes</li>
               <li>Be active</li>
               <li>Fix Healthy Snacks</li>
               <li>Get To Know Food Labels</li>
               <li>Consult a Registerd Dietitian</li>
               <li>Follow Food Safety Guidelines</li>
               <li>Get Cooking</li>
               <li>Dine Out without Ditching Your Goals</li>
               <li>Banish Brown Bag Boredom</li>
               <li>Drink More Water</li>
               <li>Explore New Foods and Flavors</li>
              </ul>
              <a href="pdf_files/14_HealthTips-for-2014.pdf" class="button top-5">Download PDF</a> </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
