<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Trainings</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li class="current"><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-3">
              <h2 class="p3"><span class="color-1">Main</span> Exercises</h2>
              <p class="p2"><strong>Our main exercises atSam_Ndi Gym</strong></p>
              <p class="p4">Here is a list of many but not all exercise activities we do at Sam_Ndi Gym:</p>
              <ul class="list-2 p5">
                <li><a href="#">Around the worlds</a></li>
                <li><a href="#">Arm circles</a></li>
                <li><a href="#">Around dumbbell press</a></li>
                <li><a href="#">Ankle on the knee</a></li>
                <li><a href="#">Axle deadlifts</a></li>
              </ul>
              <p>A fool list of exercises is offered on ourtimetable schedule.Feel free to check out!</p>
              <a href="timetable.php" class="button top-3">Check Out</a> </div>
            <div class="col-4">
              <h2 class="p3"><span class="color-1">Our</span> training</h2>
              
              <p class="p2"><strong>The Twice per week Cross-Country Run</strong></p>
              <p>We provide a 8 km run twice per week making a one trip around the town.All members are advised to take part as it provide one of the biggest mechanisms to lose fats.</p>
              <div class="wrap box-1 top-4"> <img src="images/training-img1.jpg" alt="" class="img-border img-indent">
                <div class="extra-wrap">
                  <br><p class="p2"><strong>The Big Muscle Plan</strong></p>
                  <p>It is a dream of every man to posses big muscles as it makes them feel more attractive to ladies.At Sam_Ndi Gym,we ensure that we fulfil your dreams to have big muscles throughour advanced trainings and activities.</p>
                </div>
                <div class="wrap box-1 top-4"> <img src="images/training-img2.jpg" alt="" class="img-border img-indent">
              </div>
                <div class="extra-wrap">
                  <br><p class="p2"><strong>A Formular For fats Loss</strong></p>
                  <p>Apart from the outside run,all our trainings are aimed at reducing and burning off of the excess fats from your body.We do jogging,weight lifting,rolling,abducting,etc..</p>
                </div>
                <div class="wrap box-1 top-2"> <img src="images/training-img3.jpg" alt="" class="img-border img-indent">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
