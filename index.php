<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/slider.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/tms-0.3.js"></script>
<script src="js/tms_presets.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<script>
$(window).load(function(){
	$('.slider')._TMS({
	prevBu:'.prev',
	nextBu:'.next',
	pauseOnHover:true,
	pagNums:false,
	duration:800,
	easing:'easeOutQuad',
	preset:'Fade',
	slideshow:7000,
	pagination:'.pagination',
	waitBannerAnimation:false,
	banners:'fade'
	})
}) 	
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li class="current"><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
      </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="slider">
          <ul class="items">
            <li><img src="images/pic2.jpg" alt="image">
              <div class="banner">
                <p class="font-1">Special<span>Appointment</span></p>
                <p class="font-2">Wish to book  an appointment with our trainers? Or book our gymnasium hall for commercial purposes?Feel free to do so now.</p>
                <a href="book.php">Book Now</a> </div>
            </li>
            <li><img src="images/pic3.jpg" alt="image">
              <div class="banner">
                <p class="font-1">Get Free<span>Membership</span></p>
                <p class="font-2">Feel free to gain a free access to our membership file.Just fill in a simple registration form and its done!</p>
                <a href="register.php">Become a Member</a> </div>
            </li>
            <li><img src="images/pic1.jpg" alt="image">
              <div class="banner">
                <p class="font-1">Discussion<span>Forum</span></p>
                <p class="font-2">Do you have something that you would like to share with your fellow trainees or our staff?Post you question or give you view on our discussion forum.</p>
                <a href="forum.php">Post Now</a> </div>
            </li>
          </ul>
          <div class="pagination">
            <ul>
              <li><a href="#"></a></li>
              <li><a href="#"></a></li>
              <li><a href="#"></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="grid_12 top-1">
        <div class="block-1 box-shadow">
          <p class="font-3">We are determined to offer a high quality gymnasium services to all our members.We also offer a chance to all those who wish to book our gymnasium hall for commercial purpose to do so online.We are a post-modern gymnasium with all current services on our menu!.All new equipments available and very proficient trainers ready to serve you.Join now and experince the new gymnasium era!.</p>
        </div>
      </div>
      <div class="grid_12 top-1">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1">
              <h2 class="p3"><span class="color-1">Upcoming</span> events</h2>
              
                <div class="extra-wrap">
                  <p class="p2"><strong>A Fee Discount For Our New Members</strong> </p>
                  <p>Our Sam_Ndi Gym is your caring partner on terms of charges.Right now we are offering a discount for every service we offer.Join now and experience our pocket-friendly charges.</p>
                </div>
                <div class="wrap box-1"> <img src="images/index-img1.jpg" alt="" class="img-border img-indent"><br>
              </div><br>
              
                <div class="extra-wrap">
                  <p><strong>A Children Section To Be constructed</strong></p>
                  <p>On our daily  duty to offer you high quality services,we have continued to ensure we are offering modern high-level environment for a worthwhile training.We are currently constructing a children section in our Sam_Ndi Gym to ensure your whole family is catered for.Free free to pay a visit to our gymnasium area to be a witness and give us your feed back</p>
                </div>
                <div class="wrap box-1 top-2"> <img src="images/index-img2.jpg" alt="" class="img-border img-indent">
              </div>
            </div>
            <div class="col-2">
              <h2 class="p3"><span class="color-1">Services</span> list</h2>
              <ul class="list-1">
                <li><a href="#">Main work our area</a></li>
                <li><a href="#">Cardio area/theatre</a></li>
                <li><a href="#">Group exercise classes</a></li>
                <li><a href="#">Sports facilities</a></li>
                <li><a href="#">Child care facilities</a></li>
              </ul>
              <div class="form-search">
                <h2><span class="color-1">Find us</span> near you</h2>
                <form id="form-search" method="post" action="#">
                  <input type="text" value="Enter your County" onBlur="if(this.value=='') this.value='Enter your County'" onFocus="if(this.value =='Enter your County' ) this.value=''"  />
                  <a href="#" class="search_button">Search</a>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
  <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
