<?php
require 'connect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Sam_Ndi Gym | Register | Loggin</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
<script src="js/jquery-1.7.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/cufon-yui.js"></script>
<script src="js/Asap_400.font.js"></script>
<script src="js/Coolvetica_400.font.js"></script>
<script src="js/Kozuka_M_500.font.js"></script>
<script src="js/cufon-replace.js"></script>
<script src="js/FF-cash.js"></script>
<script type="text/javascript">
function validate()
{
  //Three variables to validating email format
  var emailID = document.register.email.value;
  atpos = emailID.indexOf("@");
  dotpos = emailID.lastIndexOf(".");
 
//validates the email format entered is correct
    if (atpos < 1 || ( dotpos - atpos < 2 )) 
     { 
       alert("Please Check Your Email Address Format!");
     document.register.email.focus() ;

    return false;
     }
     if (document.register.gender.value == "Choose") 
      {
         alert("Please Select Your Gender!");
      
       document.register.gender.focus() ;

       return false
       }

if(isNaN(document.register.phone.value))
 {
  document.register.phone.value="";
  var msg=document.getElementById('show');
  show.innerHTML=" *invalid phone number.";
  show.style.color ="#f00";
  var phone=document.getElementById('phonenum');
  phone.style.backgroundColor ="#f00";
  document.register.phone.focus();
  return false;

  }
  return( true ); 
}
function checkEmail()
{
    //Store the emailword field objects into variables ...
    var email = document.getElementById('myemail');
    var email2 = document.getElementById('email2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the emailword field 
    //and the confirmation field
    if(myemail.value == email2.value){
        //The emailwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct emailword 
        email2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Email Address Match!"
    }else{
        //The emailwords do not match.
        //Set the color to the bad color and
        //notify the user.
        email2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Emails Don't Match!"
    }
} 
function upperCase()
{
var x=document.getElementById("fname").value
document.getElementById("fname").value=x.toUpperCase()

var y=document.getElementById("lname").value
document.getElementById("lname").value=y.toUpperCase()
}
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<div class="main">
  <div class="bg-img"></div>
  <!--==============================header=================================-->
  <header>
    <h1><a href="index.php">Sam_Ndi<strong>Gym.</strong></a></h1>
    <nav>
      <div class="social-icons"> <a href="#" class="icon-2"></a> <a href="#" class="icon-1"></a> </div>
      <div id="navMenu">
      <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="trainings.php">Trainings</a></li>
        <li><a href="timetable.php">Timetable</a></li>
        <li><a href="nutrition.php">Nutrition</a></li>
        <li><a href="#">Members</a>
           <ul><li class="current"><a href="register.php">Login / Register</a></li><br>
               <li><a href="book.php">Book Appointment</a></li><br>
               <li><a href="profile.php">My Profile</a></li>
           </ul>
        <li><a href="forum.php">Forum</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="contacts.php">Contacts</a></li>
      </ul>
    </div>
    </nav>
  </header>
  <!--==============================content================================-->
  <section id="content">
    <div class="container_12">
      <div class="grid_12">
        <div class="box-shadow">
          <div class="wrap block-2">
            <div class="col-1">
              <h2 class="p3"><span class="color-1">Register and </span> Become a Member</h2><br><br>
               <div id="table">
               <table border="0" cellpadding="2" cellspacing="7" width="110%">
                         <form name="register" method="post" action="signup.php" enctype="multipart/form-data" onsubmit="return(validate());">
                             <tr><td  class="space">First name</td><td><div id="inputs"><input type="text" required="required" maxlength="16" name="first" id="fname" onblur="upperCase()"/></div></td></tr>
                             <tr><td class="space">Surname</td><td><div id="inputs"><input type="text" required="required" maxlength="16"  name="last" id="lname" onblur="upperCase()"/></div></td></tr>
                             <tr><td class="space">Email Address</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email" id="myemail"/></div></td></tr>
                             <tr><td class="space">Confirm Email</td><td><div id="inputs"><input type="email" required="required" maxlength="60"  name="email2"
                               id="email2" onkeyup="checkEmail(); return false;" /><span id="confirmMessage" class="confirmMessage"></span></div></td></tr>
                             <tr><td class="space">Phone no.</td><td><div id="inputs"><input type="text" required="required" maxlength="15"  name="phone" id="phonenum" /><span id="show"></span></div></td></tr>
                             <tr><td class="space">Gender</td><td>
                                      <select name="gender"> 
                                         <option selected="selected">Choose</option> 
                                         <option>Male</option> 
                                         <option>Female</option>
                                      </select>
                            </td></tr>
                            <tr><td class="space">Profile Picture</td><td><input type="file" name="file" /></td></tr>
                            <tr><td colspan="2" align="center"><br />
                              <button type="submit" id="actions">Sign Up</button></td></tr>
                        </form>
                    </table>
                  </div>
            </div>
            <div class="col-2">
              <h2 class="p6"><span class="color-1">Member</span> Login</h2><br>
                 <form action="log_in.php" method="post" name="logginForm">
                       <div id="inputs">
                         <label for="email">Email
                         <input id="username" type="email" name="email" placeholder="Your email address" required/></label><br><br>
                         <label for="password">Password            
                         <br><input id="password" type="password" name="password" placeholder="Password" required/></label>
                      </div>
    
                     
                         <button type="submit" id="actions">Log in</button><br>
                         <label><input class="check" type="checkbox" checked="checked"/> Keep me signed in</label>

                  </form>

                         <?php 

                           if(!isset($_REQUEST['id'])){
                              echo "<a href='register.php?id=1'>forgot password?</a>";
                              echo "<br>";
                               }

                          elseif(isset($_REQUEST['id']))
                            {
                              ?>
                        <div style="border: 1px solid #C0C0C0;padding: 5px;width: 240px;">
                            <form method="post" action="" >
                               Your Email<div id="inputs"><input type="email" required="required" maxlength="60"  name="emailadd"/></div>
                                  <button type="submit" class="search" name="submit">Search</button>
                            </form>
                  
                          <?php

                              if(isset($_POST['submit']))
                                 {
       
                                 $emailadd=$_POST['emailadd'];

                                 $find="SELECT email,fname,lname FROM users WHERE email='$emailadd'";

                                 $result=mysql_query($find) or die(mysql_error());


                            if(mysql_num_rows($result)==0)
                               {
                                echo "<div class='wrong_email'>*The Email Address Entered is Invalid!!</div>";
                               }
                            else
                              {
                                function randomPass($length)
                                  {
                                    $password="";
                                    $length=$length;
                                    $chars="abcdefghijklmnopqrstuvwxyz123456789";

                                  for($x=1;$x<=$length;$x++)
           
                                     {
                                    $rand=rand() %strlen($chars);
                                    $temp=substr($chars,$rand,1);
                                    $password.=$temp;
                                    }
                                return $password;
                               }
  
  
                $pass=randomPass(7);
                //$pas1=md5($pass);

               
                  $query="UPDATE users SET password='$pass' WHERE email='$emailadd'";
                  $result1 = mysql_query($query);
                   if(!$query)
                        {
                            echo "Failed to update ".mysql_error();
                         }

                  while($row=mysql_fetch_array($result))
                     {
                      $name=$row['fname'];

                  $to = $emailadd;
                  $user=$name;
                  $subject = "New Login Password";
                  $header="Admin sam_ndi gymnasiums";
                  $message = "Hello $user ,\nYour New Password is $pass .\n\nPlease call us if your are not aware of the password change.\n\n\nThis is automatically sent Email.Please do not reply.";
                  
                  mail($to,$subject,$message,$header);
                      
                      echo "<div class='showname'>Hello ".$row['fname'] ." ".$row['lname']."</div>";
                      echo "<div class='showdetails'>New <span style='color:#ff6666;font-style:italic;'>Password</span> has been sent to Your Email.Please use it to Login now.</div>";
                  

                 }
               }
             }
                            }
                         ?>
                </div>
                             <br><br>
                   
                </form>
               </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  <!--==============================footer=================================-->
 <footer>
    <p>© 2014 Sam_Ndi Gym</p>
    <p>Website by <a target="_blank" href="https://plus.google.com/111676542148226690050/" class="link">Sam Ndi and CO.</a></p>
  </footer>
</div>
<script>Cufon.now();</script>
</body>
</html>
